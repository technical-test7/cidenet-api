package co.com.cidenet.management.service;

import java.util.List;
import co.com.cidenet.management.exception.EmployeeNotFoundException;
import co.com.cidenet.management.exception.EmployeeValidationException;
import co.com.cidenet.management.model.EmployeeDTO;
import co.com.cidenet.management.model.entity.Employee;

public interface EmployeeService {

    List<Employee> findByPageAndCriteria(int page, int size, String... criteria);

    Employee save(EmployeeDTO employee) throws EmployeeValidationException;

    Employee update(EmployeeDTO employee) throws EmployeeValidationException, EmployeeNotFoundException;

    void delete(EmployeeDTO employee) throws EmployeeNotFoundException;

}
