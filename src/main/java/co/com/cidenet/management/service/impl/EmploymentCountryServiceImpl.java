package co.com.cidenet.management.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.com.cidenet.management.model.entity.EmploymentCountry;
import co.com.cidenet.management.repository.EmploymentCountryRepository;
import co.com.cidenet.management.service.EmploymentCountryService;

@Service
public class EmploymentCountryServiceImpl implements EmploymentCountryService {

    @Autowired
    private EmploymentCountryRepository repository;

    @Override
    public Iterable<EmploymentCountry> findAll() {
        return repository.findAll();
    }

}
