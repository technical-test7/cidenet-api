package co.com.cidenet.management.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.com.cidenet.management.model.entity.EmploymentArea;
import co.com.cidenet.management.repository.EmploymentAreaRepository;
import co.com.cidenet.management.service.EmploymentAreaService;

@Service
public class EmploymentAreaServiceImpl implements EmploymentAreaService {

    @Autowired
    private EmploymentAreaRepository repository;

    @Override
    public Iterable<EmploymentArea> findAll() {
        return repository.findAll();
    }

}
