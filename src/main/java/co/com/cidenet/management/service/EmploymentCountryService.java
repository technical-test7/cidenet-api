package co.com.cidenet.management.service;

import co.com.cidenet.management.model.entity.EmploymentCountry;

public interface EmploymentCountryService {
    
    Iterable<EmploymentCountry> findAll();

}
