package co.com.cidenet.management.service;

import co.com.cidenet.management.model.entity.EmploymentArea;

public interface EmploymentAreaService {
    
    Iterable<EmploymentArea> findAll();
    
}
