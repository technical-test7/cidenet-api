package co.com.cidenet.management.service;

import co.com.cidenet.management.model.entity.IdentificationType;

public interface IdentificationTypeService {
    
    Iterable<IdentificationType> findAll();

}
