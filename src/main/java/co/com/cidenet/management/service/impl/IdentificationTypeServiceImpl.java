package co.com.cidenet.management.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.com.cidenet.management.model.entity.IdentificationType;
import co.com.cidenet.management.repository.IdentificationTypeRepository;
import co.com.cidenet.management.service.IdentificationTypeService;

@Service
public class IdentificationTypeServiceImpl implements IdentificationTypeService {

    @Autowired
    private IdentificationTypeRepository repository;

    @Override
    public Iterable<IdentificationType> findAll() {
        return repository.findAll();
    }

}
