package co.com.cidenet.management.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import co.com.cidenet.management.constant.DomainConstant;
import co.com.cidenet.management.exception.EmployeeNotFoundException;
import co.com.cidenet.management.exception.EmployeeValidationException;
import co.com.cidenet.management.model.EmployeeDTO;
import co.com.cidenet.management.model.entity.Employee;
import co.com.cidenet.management.repository.EmployeeRepository;
import co.com.cidenet.management.service.EmployeeService;
import static co.com.cidenet.management.constant.ErrorConstant.*;
import static co.com.cidenet.management.repository.spec.EmployeeSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> findByPageAndCriteria(int page, int size, String... criteria) {
        final var firstName = criteria[0];

        final var middleName = criteria[1];

        final var firstLastname = criteria[2];

        final var middleLastname = criteria[3];

        final var identificationType = null != criteria[4] ? Integer.parseInt(criteria[4]) : null;

        final var identificationNumber = criteria[5];

        final var employmentCountry = criteria[6];

        final var email = criteria[7];

        final var status = criteria[8];

        var employeeSpecification = where(byGreaterThanId(0));

        if (null != firstName) {
            employeeSpecification = employeeSpecification.and(byFirstName(firstName.toUpperCase()));
        }

        if (null != middleName) {
            employeeSpecification = employeeSpecification.and(byMiddleName(middleName.toUpperCase()));
        }

        if (null != firstLastname) {
            employeeSpecification = employeeSpecification.and(byFirstLastname(firstLastname.toUpperCase()));
        }

        if (null != middleLastname) {
            employeeSpecification = employeeSpecification.and(byMiddleLastname(middleLastname.toUpperCase()));
        }

        if (null != identificationType) {
            employeeSpecification = employeeSpecification.and(byIdType(identificationType));
        }

        if (null != identificationNumber) {
            employeeSpecification = employeeSpecification.and(byIdNumber(identificationNumber));
        }

        if (null != employmentCountry) {
            employeeSpecification = employeeSpecification.and(byEmploymentCountry(employmentCountry));
        }

        if (null != email) {
            employeeSpecification = employeeSpecification.and(byEmail(email));
        }

        if (null != status) {
            employeeSpecification = employeeSpecification.and(byStatus(status));
        }

        final var employeePage = repository.findAll(employeeSpecification, PageRequest.of(page, size));

        return employeePage.getContent();
    }

    @Override
    public Employee save(EmployeeDTO employeeDTO) throws EmployeeValidationException {
        validateEmployeeData(employeeDTO);

        final var idTypeExists = repository.findOne(byIdType(employeeDTO.identificationType())).isPresent();

        final var idNumberExists = repository.findOne(byIdNumber(employeeDTO.identificationNumber())).isPresent();

        if (idTypeExists && idNumberExists) {
            throw new EmployeeValidationException(EMPLOYEE_ID_TYPE_AND_NUMBER_EXISTS.getDescription());
        }

        final var employee = new Employee();

        final var email = generateEmail(employeeDTO.firstName(), employeeDTO.firstLastname(), employeeDTO.employmentCountry());

        final var registryDate = LocalDateTime.parse(employeeDTO.registryDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

        employee.setFirstName(employeeDTO.firstName().toUpperCase());
        employee.setMiddleName(employeeDTO.middleName().toUpperCase());
        employee.setFirstLastname(employeeDTO.firstLastname().toUpperCase());
        employee.setMiddleLastname(employeeDTO.middleLastname().toUpperCase());
        employee.setEmploymentCountry(employeeDTO.employmentCountry());
        employee.setIdentificationType(employeeDTO.identificationType());
        employee.setIdentificationNumber(employeeDTO.identificationNumber());
        employee.setEmail(email);
        employee.setStartDate(employeeDTO.startDate());
        employee.setEmploymentArea(employeeDTO.employmentArea());
        employee.setStatus(employeeDTO.status());
        employee.setRegistryDate(registryDate.toString());

        return repository.save(employee);
    }

    @Override
    public Employee update(EmployeeDTO employeeDTO)
            throws EmployeeValidationException, EmployeeNotFoundException {
        validateEmployeeData(employeeDTO);

        final var employeeOptional = repository.findById(employeeDTO.sequential());

        final var employee = employeeOptional.orElseThrow(
                () -> new EmployeeNotFoundException(EMPLOYEE_NOT_EXISTS.getDescription()));

        final var hasSameName = employee.getFirstName().equalsIgnoreCase(employeeDTO.firstName());

        final var hasSameLastname = employee.getFirstLastname().equalsIgnoreCase(employeeDTO.firstLastname());

        if (!(hasSameName && hasSameLastname)) {
            final var email = generateEmail(employeeDTO.firstName(), employeeDTO.firstLastname(), employeeDTO.employmentCountry());
            
            employee.setEmail(email);
        }

        employee.setFirstName(employeeDTO.firstName().toUpperCase());

        employee.setMiddleName(employeeDTO.middleName().toUpperCase());

        employee.setFirstLastname(employeeDTO.firstLastname().toUpperCase());

        employee.setMiddleLastname(employeeDTO.middleLastname().toUpperCase());

        employee.setEmploymentCountry(employeeDTO.employmentCountry());

        employee.setIdentificationType(employeeDTO.identificationType());

        employee.setIdentificationNumber(employeeDTO.identificationNumber());

        employee.setStartDate(employeeDTO.startDate());

        employee.setEmploymentArea(employeeDTO.employmentArea());
        
        employee.setStatus(employeeDTO.status());

        return repository.save(employee);
    }

    @Override
    public void delete(EmployeeDTO employeeDTO) throws EmployeeNotFoundException {
        final var employeeOptional = repository.findById(employeeDTO.sequential());

        var employee = employeeOptional.orElseThrow(
                () -> new EmployeeNotFoundException(EMPLOYEE_NOT_EXISTS.getDescription()));

        repository.delete(employee);
    }

    private String generateEmail(String firstName, String firstLastname, String employmentCountry)
            throws EmployeeValidationException {
        firstName = firstName.toLowerCase();

        firstLastname = firstLastname.replace(" ", "").toLowerCase();

        var id = 0;

        final var domain = DomainConstant.getConstantByCountry(employmentCountry).getDomain();

        final var email = new StringBuilder(firstName + '.' + firstLastname + '@' + domain);

        while (repository.findOne(byEmail(email.toString())).isPresent()) {
            email.delete(0, email.length());
            email.append(firstName);
            email.append('.');
            email.append(firstLastname);
            email.append('.');
            email.append(++id);
            email.append('@');
            email.append(domain);
        }

        return email.toString();
    }

    private void validateEmployeeData(EmployeeDTO employeeDTO) throws EmployeeValidationException {
        final var nameRegex = "[A-Z]+";

        final var nameWithSpaceRegex = "[A-Z\\s]+";

        final var idNumberRegex = "[0-9a-zA-Z\\-]+";

        final var firstName = employeeDTO.firstName().toUpperCase();

        if (!Pattern.matches(nameRegex, firstName)) {
            throw new EmployeeValidationException(EMPLOYEE_FIRSTNAME_FORMAT.getDescription());
        }

        if (firstName.length() > 20) {
            throw new EmployeeValidationException(EMPLOYEE_FIRSTNAME_LENGTH.getDescription());
        }

        final var middleName = employeeDTO.middleName().toUpperCase();

        if (!Pattern.matches(nameWithSpaceRegex, middleName)) {
            throw new EmployeeValidationException(EMPLOYEE_MIDDLENAME_FORMAT.getDescription());
        }

        if (middleName.length() > 50) {
            throw new EmployeeValidationException(EMPLOYEE_MIDDLENAME_LENGTH.getDescription());
        }

        final var firstLastname = employeeDTO.firstLastname().toUpperCase();

        if (!Pattern.matches(nameWithSpaceRegex, firstLastname)) {
            throw new EmployeeValidationException(EMPLOYEE_FIRSTLASTNAME_FORMAT.getDescription());
        }

        if (firstLastname.length() > 20) {
            throw new EmployeeValidationException(EMPLOYEE_FIRSTLASTNAME_LENGTH.getDescription());
        }

        final var middleLastname = employeeDTO.middleLastname().toUpperCase();

        if (!Pattern.matches(nameWithSpaceRegex, middleLastname)) {
            throw new EmployeeValidationException(EMPLOYEE_MIDDLELASTNAME_FORMAT.getDescription());
        }

        if (middleLastname.length() > 20) {
            throw new EmployeeValidationException(EMPLOYEE_MIDDLELASTNAME_LENGTH.getDescription());
        }

        final var identificationNumber = employeeDTO.identificationNumber();

        if (!Pattern.matches(idNumberRegex, identificationNumber)) {
            throw new EmployeeValidationException(EMPLOYEE_ID_NUMBER_FORMAT.getDescription());
        }

        if (identificationNumber.length() > 20) {
            throw new EmployeeValidationException(EMPLOYEE_ID_NUMBER_LENGTH.getDescription());
        }

        final var startDate = LocalDate.parse(employeeDTO.startDate());

        final var today = LocalDate.now();

        if (startDate.isAfter(today)) {
            throw new EmployeeValidationException(EMPLOYEE_START_DATE_AFTER.getDescription());
        }

        if (startDate.isBefore(today.minusDays(30))) {
            throw new EmployeeValidationException(EMPLOYEE_START_DATE_BEFORE.getDescription());
        }
    }

}
