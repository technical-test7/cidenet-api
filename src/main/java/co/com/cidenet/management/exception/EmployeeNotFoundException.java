package co.com.cidenet.management.exception;

public class EmployeeNotFoundException extends Exception {
    
    public EmployeeNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}
