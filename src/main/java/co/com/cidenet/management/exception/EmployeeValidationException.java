package co.com.cidenet.management.exception;

public class EmployeeValidationException extends Exception {

    public EmployeeValidationException(String errorMessage) {
        super(errorMessage);
    }

}
