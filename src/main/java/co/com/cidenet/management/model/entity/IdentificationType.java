package co.com.cidenet.management.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "identificacion_tipo")
public class IdentificationType {
    
    @Id
    @Column(name = "secuencial", nullable = false)
    private Integer sequential;

    @Column(name = "nombre", nullable = false)
    private String name;

}
