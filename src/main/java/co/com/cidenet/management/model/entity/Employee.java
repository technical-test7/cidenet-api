package co.com.cidenet.management.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "empleado")
public class Employee {

    @Id
    @Column(name = "secuencial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer sequential;

    @Column(name = "primer_nombre", nullable = false)
    private String firstName;

    @Column(name = "otros_nombres")
    private String middleName;

    @Column(name = "primer_apellido", nullable = false)
    private String firstLastname;

    @Column(name = "segundo_apellido", nullable = false)
    private String middleLastname;

    @Column(name = "pais_empleo", nullable = false)
    private String employmentCountry;

    @Column(name = "id_tipo", nullable = false)
    private Integer identificationType;

    @Column(name = "id_numero", nullable = false)
    private String identificationNumber;

    @Column(name = "correo")
    private String email;

    @Column(name = "fecha_ingreso")
    private String startDate;

    @Column(name = "area_empleo", nullable = false)
    private Integer employmentArea;

    @Column(name = "estado")
    private String status;

    @Column(name = "fecha_registro")
    private String registryDate;

}
