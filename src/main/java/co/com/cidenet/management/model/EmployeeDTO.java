package co.com.cidenet.management.model;

public record EmployeeDTO(

        Integer sequential,
        
        String firstName,

        String middleName,

        String firstLastname,

        String middleLastname,

        String employmentCountry,

        Integer identificationType,

        String identificationNumber,

        String email,

        String startDate,

        Integer employmentArea,

        String status,

        String registryDate

) {
}
