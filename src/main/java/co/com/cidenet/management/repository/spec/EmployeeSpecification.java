package co.com.cidenet.management.repository.spec;

import org.springframework.data.jpa.domain.Specification;
import co.com.cidenet.management.model.entity.Employee;

public class EmployeeSpecification {

    private EmployeeSpecification(){}

    public static Specification<Employee> byGreaterThanId(Integer id) {
        return (employee, cq, cb) -> cb.greaterThan(employee.get("sequential"), id);
    }

    public static Specification<Employee> byFirstName(String firstName) {
        return (employee, cq, cb) -> cb.equal(employee.get("firstName"), firstName);
    }

    public static Specification<Employee> byMiddleName(String middleName) {
        return (employee, cq, cb) -> cb.equal(employee.get("middleName"), middleName);
    }

    public static Specification<Employee> byFirstLastname(String firstLastname) {
        return (employee, cq, cb) -> cb.equal(employee.get("firstLastname"), firstLastname);
    }

    public static Specification<Employee> byMiddleLastname(String middleLastname) {
        return (employee, cq, cb) -> cb.equal(employee.get("middleLastname"), middleLastname);
    }

    public static Specification<Employee> byIdType(Integer identificationType) {
        return (employee, cq, cb) -> cb.equal(employee.get("identificationType"), identificationType);
    }

    public static Specification<Employee> byIdNumber(String identificationNumber) {
        return (employee, cq, cb) -> cb.equal(employee.get("identificationNumber"), identificationNumber);
    }

    public static Specification<Employee> byEmploymentCountry(String employmentCountry) {
        return (employee, cq, cb) -> cb.equal(employee.get("employmentCountry"), employmentCountry);
    }

    public static Specification<Employee> byEmail(String email) {
        return (employee, cq, cb) -> cb.equal(employee.get("email"), email);
    }

    public static Specification<Employee> byStatus(String status) {
        return (employee, cq, cb) -> cb.equal(employee.get("status"), status);
    }

}
