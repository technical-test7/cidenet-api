package co.com.cidenet.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import co.com.cidenet.management.model.entity.EmploymentArea;

@Repository
public interface EmploymentAreaRepository extends CrudRepository<EmploymentArea, Integer> {}
