package co.com.cidenet.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import co.com.cidenet.management.model.entity.EmploymentCountry;

@Repository
public interface EmploymentCountryRepository extends CrudRepository<EmploymentCountry, String> {
}
