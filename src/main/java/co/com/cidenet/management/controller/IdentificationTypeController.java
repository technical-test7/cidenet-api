package co.com.cidenet.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.com.cidenet.management.model.entity.IdentificationType;
import co.com.cidenet.management.service.IdentificationTypeService;

@RestController
@RequestMapping("/api/identification/type")
public class IdentificationTypeController {

    @Autowired
    private IdentificationTypeService service;

    @GetMapping("/find")
    Iterable<IdentificationType> find() {
        return service.findAll();
    }

}
