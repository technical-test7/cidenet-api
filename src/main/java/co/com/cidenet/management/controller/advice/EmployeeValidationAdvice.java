package co.com.cidenet.management.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import co.com.cidenet.management.exception.EmployeeValidationException;

@ControllerAdvice
public class EmployeeValidationAdvice {

    @ResponseBody
    @ExceptionHandler(EmployeeValidationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String employeeNotFoundHandler(EmployeeValidationException ex) {
        return ex.getMessage();
    }

}
