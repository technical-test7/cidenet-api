package co.com.cidenet.management.controller;

import co.com.cidenet.management.exception.EmployeeNotFoundException;
import co.com.cidenet.management.exception.EmployeeValidationException;
import co.com.cidenet.management.model.EmployeeDTO;
import co.com.cidenet.management.model.entity.Employee;
import co.com.cidenet.management.service.EmployeeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/employee")
class EmployeeController {

    @Autowired
    private EmployeeService service;

    @GetMapping("/find/criteria")
    List<Employee> find(@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String middleName,
            @RequestParam(required = false) String firstLastname,
            @RequestParam(required = false) String middleLastname,
            @RequestParam(required = false) String identificationType,
            @RequestParam(required = false) String identificationNumber,
            @RequestParam(required = false) String employmentCountry,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String status) {
        return service.findByPageAndCriteria(page, size, firstName, middleName, firstLastname, middleLastname,
                identificationType, identificationNumber, employmentCountry, email, status);
    }

    @PostMapping("/save")
    Employee save(@RequestBody EmployeeDTO employeeDTO) throws EmployeeValidationException {
        return service.save(employeeDTO);
    }

    @PutMapping("/update")
    Employee update(@RequestBody EmployeeDTO employeeDTO)
            throws EmployeeValidationException, EmployeeNotFoundException {
        return service.update(employeeDTO);
    }

    @DeleteMapping("/delete")
    void delete(@RequestBody EmployeeDTO employeeDTO) throws EmployeeNotFoundException {
        service.delete(employeeDTO);
    }

}
