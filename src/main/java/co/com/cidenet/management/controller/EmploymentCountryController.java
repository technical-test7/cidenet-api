package co.com.cidenet.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.com.cidenet.management.model.entity.EmploymentCountry;
import co.com.cidenet.management.service.EmploymentCountryService;

@RestController
@RequestMapping("/api/employment/country")
public class EmploymentCountryController {

    @Autowired
    private EmploymentCountryService service;

    @GetMapping("/find")
    Iterable<EmploymentCountry> find() {
        return service.findAll();
    }
    
}
