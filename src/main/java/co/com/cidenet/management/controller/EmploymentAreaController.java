package co.com.cidenet.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.com.cidenet.management.model.entity.EmploymentArea;
import co.com.cidenet.management.service.EmploymentAreaService;

@RestController
@RequestMapping("/api/employment/area")
public class EmploymentAreaController {

    @Autowired
    private EmploymentAreaService service;

    @GetMapping("/find")
    Iterable<EmploymentArea> find() {
        return service.findAll();
    }

}
