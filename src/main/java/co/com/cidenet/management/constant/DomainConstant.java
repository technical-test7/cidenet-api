package co.com.cidenet.management.constant;

import java.util.Arrays;
import co.com.cidenet.management.exception.EmployeeValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import static co.com.cidenet.management.constant.ErrorConstant.*;

@AllArgsConstructor
@Getter
public enum DomainConstant {

    CO_DOMAIN("CO", "cidenet.com.co"), US_DOMAIN("US", "cidenet.com.us");

    private final String country;
    private final String domain;

    public static DomainConstant getConstantByCountry(String country)
            throws EmployeeValidationException {
        final var constants = Arrays.asList(DomainConstant.values()).parallelStream();

        return constants.filter(constant -> constant.country.equals(country)).findFirst()
                .orElseThrow(() -> new EmployeeValidationException(EMPLOYMENT_COUNTRY_NOT_EXISTS.getDescription()));
    }

}
