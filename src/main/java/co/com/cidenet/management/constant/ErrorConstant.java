package co.com.cidenet.management.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorConstant {
    
    EMPLOYEE_NOT_EXISTS("El empleado no existe en la base de datos."),
    EMPLOYEE_FIRSTNAME_FORMAT("El primer nombre del empleado solo puede contener caracteres desde la A hasta la Z, excluyendo la letra Ñ."),
    EMPLOYEE_FIRSTNAME_LENGTH("El primer nombre del empleado solo puede contener hasta 20 caracteres de longitud."),
    EMPLOYEE_MIDDLENAME_FORMAT("Los otros nombres del empleado solo pueden contener caracteres desde la A hasta la Z, excluyendo la letra Ñ."),
    EMPLOYEE_MIDDLENAME_LENGTH("Los otros nombres del empleado solo pueden contener hasta 50 caracteres de longitud."),
    EMPLOYEE_FIRSTLASTNAME_FORMAT("El primer apellido del empleado solo puede contener caracteres desde la A hasta la Z, excluyendo la letra Ñ."),
    EMPLOYEE_FIRSTLASTNAME_LENGTH("El primer apellido del empleado solo puede contener hasta 20 caracteres de longitud."),
    EMPLOYEE_MIDDLELASTNAME_FORMAT("El segundo apellido del empleado solo puede contener caracteres desde la A hasta la Z, excluyendo la letra Ñ."),
    EMPLOYEE_MIDDLELASTNAME_LENGTH("El segundo apellido del empleado solo puede contener hasta 20 caracteres de longitud."),
    EMPLOYEE_ID_NUMBER_FORMAT("El numero de identificacion del empleado solo puede contener caracteres desde la A hasta la Z, incluyendo el signo '-' y excluyendo la letra Ñ."),
    EMPLOYEE_ID_NUMBER_LENGTH("El numero de identificacion del empleado solo puede contener hasta 20 caracteres de longitud."),
    EMPLOYEE_ID_TYPE_AND_NUMBER_EXISTS("Ya existe un empleado con el mismo tipo y numero de identificacion."),
    EMPLOYEE_EMAIL_LENGTH("El correo electronico del empleado solo puede contener hasta 300 caracteres de longitud."),
    EMPLOYEE_START_DATE_AFTER("La fecha de ingreso del empleado no puede ser mayor a la fecha actual."),
    EMPLOYEE_START_DATE_BEFORE("La fecha de ingreso del empleado no puede tener mas de 30 dias de antiguedad sobre la fecha actual."),
    EMPLOYMENT_COUNTRY_NOT_EXISTS("El pais de empleo no se encuentra parametrizado.");

    private final String description;
    
}
