const fillRegistryDate = () => {
    const registryDateElement = document.getElementById('input-registry-date')

    const date = new Date()

    const dateOptions = {
        hour12: false,
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }

    const registryDate = date.toLocaleString('es-es', dateOptions)

    registryDateElement.value = registryDate.replace(',', '')
}

const populateEmploymentCountry = async() => {
    const selectElement = document.getElementById('input-employment-country')
    const response = await fetch('http://localhost:8080/api/employment/country/find')
    const employmentCountries = await response.json()
    
    for (const employmentCountry of employmentCountries) {
        let optionElement = document.createElement('option')
        optionElement.value = employmentCountry.code
        optionElement.innerHTML = employmentCountry.name

        selectElement.appendChild(optionElement)
    }
}

const populateIdType = async() => {
    const selectElement = document.getElementById('input-id-type')
    const response = await fetch('http://localhost:8080/api/identification/type/find')
    const idTypes = await response.json()
    
    for (const idType of idTypes) {
        let optionElement = document.createElement('option')
        optionElement.value = idType.sequential
        optionElement.innerHTML = idType.name

        selectElement.appendChild(optionElement)
    }
}

const populateEmploymentArea = async() => {
    const selectElement = document.getElementById('input-employment-area')
    const response = await fetch('http://localhost:8080/api/employment/area/find')
    const employmentAreas = await response.json()
    
    for (const employmentArea of employmentAreas) {
        let optionElement = document.createElement('option')
        optionElement.value = employmentArea.sequential
        optionElement.innerHTML = employmentArea.name

        selectElement.appendChild(optionElement)
    }
}

fillRegistryDate()
populateEmploymentCountry()
populateIdType()
populateEmploymentArea()

const submitButtonElement = document.getElementById('submit-btn');

const inputs = [...document.querySelectorAll('input[type="text"], input[type="date"]')]

inputs.forEach((input) => {
    input.addEventListener('input', function() {
      if (inputs.every(input => input.value.trim())) {
        submitButtonElement.removeAttribute('disabled')
      }
    });
});

submitButtonElement.onclick = function() {
    const firstName = document.getElementById('input-first-name')
    const middleName = document.getElementById('input-middle-name')
    const firstLastname = document.getElementById('input-first-lastname')
    const middleLastname = document.getElementById('input-middle-lastname')
    const identificationNumber = document.getElementById('input-id-number')
    const identificationType = document.getElementById('input-id-type')
    const employmentArea = document.getElementById('input-employment-area')
    const employmentCountry = document.getElementById('input-employment-country')
    const startDate = document.getElementById('input-start-date')
    const registryDate= document.getElementById('input-registry-date')

    const data = {
        firstName: firstName.value,
        middleName: middleName.value,
        firstLastname: firstLastname.value,
        middleLastname: middleLastname.value,
        identificationNumber: identificationNumber.value,
        identificationType: identificationType.value,
        employmentArea: employmentArea.value,
        employmentCountry: employmentCountry.value,
        startDate: startDate.value,
        registryDate: registryDate.value,
        status: 'A'
    }

    submitForm(data)
}

const submitForm = (data) => {
    const promise = fetch('http://localhost:8080/api/employee/save', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset="utf-8"'
            },
            body: JSON.stringify(data)
    })

    console.log(promise)
}
