create table pais_empleo (
    codigo char(2),
    nombre varchar(255) not null,
    constraint pais_pk primary key (codigo)
);

insert into pais_empleo (codigo, nombre)
values ('CO', 'COLOMBIA');

insert into pais_empleo (codigo, nombre)
values ('US', 'ESTADOS UNIDOS');

create table identificacion_tipo (
    secuencial tinyint generated always as identity,
    nombre varchar(255) not null,
    constraint identificacion_pk primary key (secuencial)
);

insert into identificacion_tipo (nombre)
values ('CEDULA DE CIUDADANIA');

insert into identificacion_tipo (nombre)
values ('CEDULA DE EXTRANJERIA');

insert into identificacion_tipo (nombre)
values ('PASAPORTE');

insert into identificacion_tipo (nombre)
values ('PERMISO ESPECIAL');

create table area_empleo (
    secuencial tinyint generated always as identity,
    nombre varchar(255) not null,
    constraint area_pk primary key (secuencial)
);

insert into area_empleo (nombre)
values ('ADMINISTRACION');

insert into area_empleo (nombre)
values ('FINANCIERA');

insert into area_empleo (nombre)
values ('COMPRAS');

insert into area_empleo (nombre)
values ('INFRAESTRUCTURA');

insert into area_empleo (nombre)
values ('OPERACION');

insert into area_empleo (nombre)
values ('TALENTO HUMANO');

insert into area_empleo (nombre)
values ('SERVICIOS VARIOS');

create table empleado (
    secuencial int generated always as identity,
    primer_nombre varchar(20) not null,
    otros_nombres varchar(50),
    primer_apellido varchar(20) not null,
    segundo_apellido varchar(20) not null,
    pais_empleo char(2) not null,
    id_tipo tinyint not null,
    id_numero varchar(20)  not null,
    correo varchar(300),
    fecha_ingreso date,
    area_empleo tinyint not null,
    estado char(1) not null,
    fecha_registro timestamp default current_timestamp,
    constraint empleado_pk primary key (secuencial),
    constraint empleado_correo_unq unique (correo),
    constraint empleado_id_numero_unq unique (id_numero)
);

insert into empleado (
    primer_nombre,
    otros_nombres,
    primer_apellido,
    segundo_apellido,
    pais_empleo,
    id_tipo,
    id_numero,
    correo,
    fecha_ingreso,
    area_empleo,
    estado
) values (
    'TORIBIO',
    'EMANUAL',
    'DOMINGUEZ',
    'RODRIGUEZ',
    'CO',
    1,
    '1111111111',
    'toribio.dominguez@cidenet.com.co',
    current_date,
    7,
    'A'
);

insert into empleado (
    primer_nombre,
    otros_nombres,
    primer_apellido,
    segundo_apellido,
    pais_empleo,
    id_tipo,
    id_numero,
    correo,
    fecha_ingreso,
    area_empleo,
    estado
) values (
    'ROSENDO',
    'ESTEBAN',
    'CASTRO',
    'BOYD',
    'CO',
    1,
    '2222222222',
    'rosendo.castro@cidenet.com.co',
    current_date,
    3,
    'A'
);

insert into empleado (
    primer_nombre,
    otros_nombres,
    primer_apellido,
    segundo_apellido,
    pais_empleo,
    id_tipo,
    id_numero,
    correo,
    fecha_ingreso,
    area_empleo,
    estado
) values (
    'NORBERTO',
    'BELISARIO',
    'ALVAREZ',
    'MARQUEZ',
    'CO',
    1,
    '3333333333',
    'norberto.alvarez@cidenet.com.co',
    current_date,
    1,
    'A'
);

insert into empleado (
    primer_nombre,
    otros_nombres,
    primer_apellido,
    segundo_apellido,
    pais_empleo,
    id_tipo,
    id_numero,
    correo,
    fecha_ingreso,
    area_empleo,
    estado
) values (
    'MARIE',
    'STEPHANIE',
    'WEBB',
    'HERNANDEZ',
    'US',
    2,
    '4444444444',
    'marie.webb@cidenet.com.us',
    current_date,
    4,
    'A'
);

insert into empleado (
    primer_nombre,
    otros_nombres,
    primer_apellido,
    segundo_apellido,
    pais_empleo,
    id_tipo,
    id_numero,
    correo,
    fecha_ingreso,
    area_empleo,
    estado
) values (
    'UBALDO',
    'JAVIER',
    'TORRES',
    'REYES',
    'US',
    3,
    'MX55555555',
    'ubaldo.torres@cidenet.com.us',
    current_date,
    4,
    'A'
);
