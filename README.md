# cidenet-api

## Description

This is an API which allows CIDENET users to create, retrieve, update and delete employees in the system.

## Requirements

This service was developed by using the following technologies:

- Java OpenJDK v17.0.3 by Temurin
- Apache Maven v3.8.3

## Run the service

In order to run this project just clone it or download it to your preferred local disk location.

```bash
git clone https://gitlab.com/technical-test7/cidenet-api.git
```

Then change the current directory to the directory where the project was cloned.

```bash
cd cidenet-api/
```

Finally run the service with the following command with maven: 

```bash
mvn spring-boot:run
```

Now the service will be running on your localhost: http://localhost:8080

## Test the service

### APIs

This service is fully integrated with OpenAPI 3.0 specifications, which you can access from this url when the service is running:
http://localhost:8080/swagger-ui/index.html

By accessing to its api specifications, you'll be able to see and test the REST endpoints.

### H2 Database

This service runs with an embedded relational database and inserts automatically 5 employees for testing purposes.

H2 Database provides a GUI which can be accesed from this url when the service is running:
http://localhost:8080/h2-console

In order to login you should fill the following fields:

- jdbc url
- user
- password

The values can be found in the ***application.properties*** file.
